var express = require('express');
var app = express();
var bodyparser = require('body-parser');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var db = require('./db');

app.use(bodyparser.json());

var port = 5000;
dbinfo = {
    host: 'chatapp.cpionotpegkz.us-east-1.rds.amazonaws.com',
    user: 'cougs_123',
    password: 'WashingtonState',
    database: 'chatapp'
}
app.route('/').post(function(req,res){
    console.log(req.body);
    if (req.body.action == "newUser"){
        delete req.body.action;
        db.insertUser(dbinfo, req.body, function(){
            res.send("New User Added!");
        });
    }
    if (req.body.action == "newMessage"){
        delete req.body.action;
        db.insertUser(dbinfo, req.body, function(){
            res.send("New Message Added!");
        });
    }
    if(req.body.action == "Messages"){
        db.getMessages(dbinfo,function(){
            res.send(db.messages);
        });
    }
});

messageHistory = [];

http.listen(port,function(){
    console.log("App has started with port: ", port);
});

io.sockets.on('connection', function(socket){
    console.log("new user connected with id <" + socket.id + '>');
    socket.emit('data', messageHistory);
    socket.join('private_room', () => {
        let rooms = Object.keys(socket.rooms);
        console.log(rooms); // [ <socket.id>, 'private_room' ]
    });
    socket.on('sendmsg', function(data){
        console.log('server: ' + data);
        messageHistory.push(String(data));
        socket.broadcast.emit('updateHeader', data);
    });
});